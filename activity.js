// Create a simple server and the following routes with their corresponding HTTP methods and responses:

let http = require("http");
let port = 4000;

http.createServer((req, res) => {
    //  If the url is http://localhost:4000/, send a response Welcome to Booking System
    if(req.url == "/" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"});

        res.end("Welcome to Booking System");
    }

    // If the url is http://localhost:4000/profile, send a response Welcome to your profile!
    if(req.url == "/profile" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"});

        res.end("Welcome to your profile!");
    }

    // If the url is http://localhost:4000/courses, send a response Here’s our courses available
    if(req.url == "/courses" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"});

        res.end("Here's our courses available");
    }

    // If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
    if(req.url == "/addcourses" && req.method == "POST"){
        res.writeHead(200, {"Content-Type": "text/plain"});

        res.end("Add a course to our resources");
    }

    // If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
    if(req.url == "/updatecourse" && req.method == "PUT"){
        res.writeHead(200, {"Content-Type": "text/plain"});

        res.end("Update a course to our resources");
    }

    // If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
    if(req.url == "/archivecourses" && req.method == "DELETE"){
        res.writeHead(200, {"Content-Type": "text/plain"});

        res.end("Archive courses to our resources");
    }


}).listen(port);